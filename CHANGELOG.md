# ChangeLog

## Unreleased

**Changed:**
* Complete refactoring

## 1.2.7 (2022-11-01)

**Changed:**
* Use fixed utilities for IanaTypes (old one caused memory issue)

## 1.2.5 (2021-11-24)

**Added:**
* Date modified annotation for datasets

## 1.2.4 (2021-10-18)

**Changed:**
* Important connector lib update

## 1.2.3 (2021-06-23)

**Changed:**
* Connector pipe handling

## 1.2.2 (2021-06-07)

**Fixed**
* Housekeeping

## 1.2.1 (2021-03-19)

**Fixed:**
* Vert.x issues (updated to 4.0.3)

## 1.2.0 (2021-01-31)

**Added:**
* `dqv:hasQualityMetadata` property for resource when metrics graph is created

**Changed:**
* Switched to Vert.x 4.0.0

## 1.1.0 (2020-04-23)

**Added:**
* Extended information about JSON-LD file-type

## 1.0.3 (2020-03-11)

**Changed:**
* Close dataset explicitly

## 1.0.2 (2020-03-06)

**Fixed:**
* Replace old measurements

## 1.0.1 (2020-03-04)

**Fixed:**
* Finally replace metrics graph to dataset

## 1.0.0 (2020-02-28)

Initial production release

**Added:**
* `attached` pipe config parameter
* Configuration `PIVEAU_LOAD_VOCABULARIES_FETCH`
* Configuration `EXCLUDE_ANNOTATIONS`
* Five Star `DQV.QualityAnnotation`
* Metric `atLeastFourStars`
* Annotate dcat ap compliance

**Changed:**
* Rearrange metadata quality
* Send rdf dataset with named graph
* Simplified pipe integration
* Use vocabularies library
* Annotations can now be excluded via configuration
* Measurements optionally replaceable
* Allow any name for metrics graph

**Removed:**
* Service proxy generation

**Fixed:**
* `dqv:computedOn`
* mediaType and format evaluation
* Test
