package io.piveau.metrics.annotator.visitors

import io.piveau.vocabularies.License
import io.piveau.vocabularies.License.altLabel
import io.piveau.vocabularies.License.exactMatch
import org.apache.jena.rdf.model.*
import org.apache.jena.vocabulary.DC_11
import org.apache.jena.vocabulary.SKOS

class LicenseKnownVisitor : RDFVisitor {
    override fun visitBlank(resource: Resource, anonId: AnonId): Boolean {
        return resource.listProperties().toList().stream().anyMatch { statement: Statement ->
            val predicate = statement.predicate
            if (predicate === DC_11.description || predicate === SKOS.prefLabel || predicate === SKOS.altLabel || predicate === SKOS.exactMatch) {
                return@anyMatch (License.getConcept(statement.literal.lexicalForm) != null
                        || altLabel(statement.literal.lexicalForm, "en") != null)
            } else {
                return@anyMatch false
            }
        }
    }

    override fun visitURI(resource: Resource, uri: String): Boolean {
        return License.isConcept(resource) || exactMatch(uri) != null
    }

    override fun visitLiteral(literal: Literal): Boolean {
        return exactMatch(literal.lexicalForm) != null || License.getConcept(literal.lexicalForm) != null || altLabel(
            literal.lexicalForm,
            "en"
        ) != null
    }
}