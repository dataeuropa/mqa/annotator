package io.piveau.metrics.annotator.visitors

import io.piveau.vocabularies.Concept
import io.piveau.vocabularies.FileType
import io.piveau.vocabularies.License
import org.apache.jena.rdf.model.*
import org.apache.jena.vocabulary.DC_11
import org.apache.jena.vocabulary.SKOS
import java.util.stream.Stream

class FormatVisitor : RDFVisitor {
    override fun visitBlank(resource: Resource, anonId: AnonId): Concept? {
        return resource.listProperties().toList().stream().flatMap { statement: Statement ->
            val predicate = statement.predicate
            if (predicate === DC_11.description || predicate === SKOS.prefLabel || predicate === SKOS.altLabel || predicate === SKOS.exactMatch) {
                return@flatMap Stream.ofNullable(License.getConcept(statement.literal.lexicalForm))
            } else {
                return@flatMap Stream.empty<Concept>()
            }
        }.findFirst().orElse(null)
    }

    override fun visitURI(resource: Resource, uri: String): Concept? {
        return FileType.getConcept(resource)
    }

    override fun visitLiteral(literal: Literal): Concept? {
        return FileType.getConcept(literal.lexicalForm)
    }
}