package io.piveau.metrics.annotator.visitors

import io.piveau.vocabularies.License
import io.piveau.vocabularies.License.altLabel
import io.piveau.vocabularies.License.exactMatch
import io.piveau.vocabularies.License.isOpen
import org.apache.jena.rdf.model.*
import org.apache.jena.vocabulary.DC_11
import org.apache.jena.vocabulary.SKOS

class LicenseOpenVisitor : RDFVisitor {
    override fun visitBlank(resource: Resource, anonId: AnonId): Boolean {
        return resource.listProperties().toList().stream().anyMatch { statement: Statement ->
            val predicate = statement.predicate
            if (predicate === DC_11.description || predicate === SKOS.prefLabel || predicate === SKOS.altLabel || predicate === SKOS.exactMatch) {
                val licence =
                    if (License.getConcept(statement.literal.lexicalForm) != null) License.getConcept(statement.literal.lexicalForm) else altLabel(
                        statement.literal.lexicalForm,
                        "en"
                    )
                return@anyMatch licence != null && isOpen(licence)
            } else {
                return@anyMatch false
            }
        }
    }

    override fun visitURI(resource: Resource, uri: String): Boolean {
        val licence = if (License.isConcept(resource)) License.getConcept(resource) else exactMatch(uri)
        return licence != null && isOpen(licence)
    }

    override fun visitLiteral(literal: Literal): Boolean {
        var licence = License.getConcept(literal.lexicalForm)
        if (licence == null) licence = exactMatch(literal.lexicalForm)
        if (licence == null) altLabel(literal.lexicalForm, "en")
        return licence != null && isOpen(licence)
    }
}