package io.piveau.metrics.annotator.annotations

import io.piveau.dqv.replaceMeasurement
import io.piveau.metrics.annotator.visitors.LicenseKnownVisitor
import io.piveau.vocabularies.vocabulary.PV
import org.apache.jena.rdf.model.Model
import org.apache.jena.rdf.model.Resource
import org.apache.jena.vocabulary.DCTerms

object KnownLicenseAnnotation : MetricsAnnotation() {

    override val metric: Resource
        get() = PV.knownLicence

    override fun annotate(resource: Resource, report: Model, annotationContext: MutableMap<String, Any>) {
        if (resource.hasProperty(DCTerms.license)) {
            val known = resource.getProperty(DCTerms.license).getObject().visitWith(LicenseKnownVisitor()) as Boolean
            report.replaceMeasurement(resource, metric, known)
        }
    }

}