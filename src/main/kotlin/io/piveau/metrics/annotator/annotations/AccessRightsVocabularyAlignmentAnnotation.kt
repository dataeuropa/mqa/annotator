package io.piveau.metrics.annotator.annotations

import io.piveau.dqv.replaceMeasurement
import io.piveau.vocabularies.AccessRight
import io.piveau.vocabularies.vocabulary.PV
import org.apache.jena.rdf.model.Model
import org.apache.jena.rdf.model.Resource
import org.apache.jena.vocabulary.DCTerms

object AccessRightsVocabularyAlignmentAnnotation : MetricsAnnotation() {

    override val metric
        get() = PV.accessRightsVocabularyAlignment

    override fun annotate(resource: Resource, report: Model, annotationContext: MutableMap<String, Any>) {
        if (resource.hasProperty(DCTerms.accessRights)) {
            val accessRights = resource.getPropertyResourceValue(DCTerms.accessRights)
            report.replaceMeasurement(
                resource,
                metric,
                accessRights != null && AccessRight.isConcept(accessRights)
            )
        }
    }

}