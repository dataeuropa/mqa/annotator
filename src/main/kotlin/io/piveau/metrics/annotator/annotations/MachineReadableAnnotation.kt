package io.piveau.metrics.annotator.annotations

import io.piveau.dqv.replaceMeasurement
import io.piveau.metrics.annotator.visitors.FormatVisitor
import io.piveau.vocabularies.Concept
import io.piveau.vocabularies.FileType.isMachineReadable
import io.piveau.vocabularies.vocabulary.PV
import org.apache.jena.rdf.model.Model
import org.apache.jena.rdf.model.Resource
import org.apache.jena.vocabulary.DCTerms

object MachineReadableAnnotation : MetricsAnnotation() {

    override val metric
        get() = PV.formatMediaTypeMachineInterpretable

    override fun annotate(resource: Resource, report: Model, annotationContext: MutableMap<String, Any>) {
        if (resource.hasProperty(DCTerms.format)) {
            val format = resource.getProperty(DCTerms.format).getObject()
            val formatConcept = format.visitWith(FormatVisitor()) as Concept?
            if (formatConcept != null) {
                val isMachineReadable = isMachineReadable(formatConcept)
                val machineReadableMeasurement =
                    report.replaceMeasurement(resource, metric, isMachineReadable)
                annotationContext["machineReadableMeasurement"] = machineReadableMeasurement
                annotationContext["isMachineReadable"] = isMachineReadable
                annotationContext["formatConcept"] = formatConcept
            }
        }
    }

}