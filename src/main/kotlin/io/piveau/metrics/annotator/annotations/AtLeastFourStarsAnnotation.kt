package io.piveau.metrics.annotator.annotations

import io.piveau.dqv.replaceMeasurement
import io.piveau.vocabularies.vocabulary.PV
import org.apache.jena.rdf.model.Model
import org.apache.jena.rdf.model.Resource

object AtLeastFourStarsAnnotation : MetricsAnnotation() {

    override val metric: Resource
        get() = PV.atLeastFourStars

    override fun annotate(resource: Resource, report: Model, annotationContext: MutableMap<String, Any>) {
        if (annotationContext.containsKey("fiveStarsRating")) {
            val fiveStarsRating = annotationContext["fiveStarsRating"] as Resource
            report.replaceMeasurement(
                resource,
                PV.atLeastFourStars,
                fiveStarsRating === PV.fourStars || fiveStarsRating === PV.fiveStars
            )
        }
    }

}