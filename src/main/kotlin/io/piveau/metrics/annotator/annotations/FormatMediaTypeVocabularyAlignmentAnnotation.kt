package io.piveau.metrics.annotator.annotations

import io.piveau.dqv.replaceMeasurement
import io.piveau.vocabularies.IanaType.isMediaType
import io.piveau.vocabularies.vocabulary.PV
import org.apache.jena.rdf.model.Model
import org.apache.jena.rdf.model.Resource
import org.apache.jena.vocabulary.DCAT

object FormatMediaTypeVocabularyAlignmentAnnotation : MetricsAnnotation() {

    override val metric: Resource
        get() = PV.formatMediaTypeVocabularyAlignment

    override fun annotate(resource: Resource, report: Model, annotationContext: MutableMap<String, Any>) {
        val knownFormat = annotationContext.containsKey("formatConcept")
        var knownMediaType = false
        if (resource.hasProperty(DCAT.mediaType)) {
            val mediaType = resource.getProperty(DCAT.mediaType).getObject()
            if (mediaType.isLiteral) {
                knownMediaType = isMediaType(mediaType.asLiteral().lexicalForm)
            } else if (mediaType.isURIResource) {
                knownMediaType = isMediaType(mediaType.asResource())
            }
        }
        report.replaceMeasurement(resource, metric, knownFormat && knownMediaType)
    }

}