package io.piveau.metrics.annotator.annotations

import io.piveau.dqv.replaceMeasurement
import io.piveau.metrics.annotator.visitors.LicenseOpenVisitor
import io.piveau.vocabularies.vocabulary.OSI
import io.piveau.vocabularies.vocabulary.PV
import org.apache.jena.rdf.model.Model
import org.apache.jena.rdf.model.Resource
import org.apache.jena.vocabulary.DCTerms

object OpenLicenseAnnotation : MetricsAnnotation() {

    override val metric: Resource
        get() = OSI.isOpen

    override fun annotate(resource: Resource, report: Model, annotationContext: MutableMap<String, Any>) {
        if (resource.hasProperty(DCTerms.license)) {
            val isOpen = resource.getProperty(DCTerms.license).getObject().visitWith(LicenseOpenVisitor()) as Boolean
            val openLicenseMeasurement = report.replaceMeasurement(resource, metric, isOpen)
            annotationContext["openLicenseMeasurement"] = openLicenseMeasurement
            annotationContext["isOpen"] = isOpen
        }
    }

}