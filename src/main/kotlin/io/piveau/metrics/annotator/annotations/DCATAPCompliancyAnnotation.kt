package io.piveau.metrics.annotator.annotations

import io.piveau.dqv.replaceMeasurement
import io.piveau.vocabularies.vocabulary.DQV
import io.piveau.vocabularies.vocabulary.PV
import io.piveau.vocabularies.vocabulary.SHACL
import org.apache.jena.rdf.model.Model
import org.apache.jena.rdf.model.RDFNode
import org.apache.jena.rdf.model.Resource
import org.apache.jena.rdf.model.Statement
import org.apache.jena.vocabulary.OA
import org.apache.jena.vocabulary.RDF
import java.util.function.Predicate

object DCATAPCompliancyAnnotation : MetricsAnnotation() {

    override val metric: Resource
        get() = PV.dcatApCompliance

    override fun annotate(resource: Resource, report: Model, annotationContext: MutableMap<String, Any>) {
        report.listStatements(resource, DQV.hasQualityAnnotation, null as RDFNode?)
            .filterKeep { it.resource.hasProperty(OA.hasBody) }
            .mapWith { it.resource.getPropertyResourceValue(OA.hasBody) }
            .filterKeep { it.hasProperty(RDF.type, SHACL.ValidationReport) }
            .nextOptional().ifPresent { body: Resource ->
                if (body.hasProperty(SHACL.conforms)) {
                    report.replaceMeasurement(resource, PV.dcatApCompliance, body.getProperty(SHACL.conforms).boolean)
                }
            }
    }

}