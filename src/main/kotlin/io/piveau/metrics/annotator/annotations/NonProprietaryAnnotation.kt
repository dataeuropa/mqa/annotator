package io.piveau.metrics.annotator.annotations

import io.piveau.dqv.replaceMeasurement
import io.piveau.vocabularies.Concept
import io.piveau.vocabularies.FileType.isNonProprietary
import io.piveau.vocabularies.vocabulary.PV
import org.apache.jena.rdf.model.Model
import org.apache.jena.rdf.model.Resource

object NonProprietaryAnnotation : MetricsAnnotation() {

    override val metric: Resource
        get() = PV.formatMediaTypeNonProprietary

    override fun annotate(resource: Resource, report: Model, annotationContext: MutableMap<String, Any>) {
        (annotationContext["formatConcept"] as Concept?)?.let { formatConcept ->
            val isNonProprietary = isNonProprietary(formatConcept)
            annotationContext["nonProprietaryMeasurement"] = report.replaceMeasurement(resource, metric, isNonProprietary)
            annotationContext["isNonProprietary"] = isNonProprietary
        }
    }

}