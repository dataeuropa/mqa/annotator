package io.piveau.metrics.annotator;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import io.piveau.metrics.ApplicationConfig;
import io.piveau.metrics.MainVerticle;
import io.piveau.pipe.PipeManager;
import io.piveau.pipe.model.DataType;
import io.piveau.pipe.model.Pipe;
import io.piveau.rdf.RDFMimeTypes;
import io.piveau.utils.JenaUtils;
import io.piveau.vocabularies.vocabulary.DQV;
import io.piveau.vocabularies.vocabulary.PROV;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Vertx;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.client.WebClient;
import io.vertx.ext.web.client.predicate.ResponsePredicate;
import io.vertx.junit5.Checkpoint;
import io.vertx.junit5.Timeout;
import io.vertx.junit5.VertxExtension;
import io.vertx.junit5.VertxTestContext;
import org.apache.jena.query.Dataset;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.riot.Lang;
import org.apache.jena.vocabulary.RDF;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

@ExtendWith(VertxExtension.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@Timeout(value = 3, timeUnit = TimeUnit.MINUTES)
class IntegrationTest {

    private static final Logger log = LoggerFactory.getLogger(IntegrationTest.class);

    private static final String TEST_NAMESPACE = "urn:junit5-tests:test-pipe";

    // TODO fix tests and add more

//    @Test
    void testPipeProcessing(Vertx vertx, VertxTestContext testContext) {

        vertx.deployVerticle(MainVerticle.class, new DeploymentOptions());

        PipeManager pipeManager = PipeManager.load("pipe-stub.json");

        Buffer data = vertx.fileSystem().readFileBlocking("dataset.ttl");

        ObjectNode dataInfo = new ObjectMapper().createObjectNode();
        dataInfo.put("identifier", "sampleDataset");
        dataInfo.put("catalogue", "any-catalogue");

        pipeManager.setPayloadData(new String(data.getBytes()), DataType.text, "application/trug", dataInfo);

        WebClient client = WebClient.create(vertx);
        client.post(8080, "localhost", "/pipe")
                .putHeader("Content-Type", "application/json")
                .expect(ResponsePredicate.SC_ACCEPTED)
                .sendJsonObject(new JsonObject(pipeManager.prettyPrint()), ar -> {
                    if (ar.succeeded()) {
                        testContext.completeNow();
                    } else {
                        testContext.failNow(ar.cause());
                    }
                });
    }

    //    @Test
    void testZeroStars(Vertx vertx, VertxTestContext testContext) {
        JsonObject config = new JsonObject().put(ApplicationConfig.ENV_PIVEAU_LOAD_VOCABULARIES_FETCH, false);

        vertx.deployVerticle(MainVerticle.class.getName(), new DeploymentOptions().setConfig(config), testContext.succeeding(deploy ->
                testDqv(vertx, testContext, "zero_star")));
    }

    //    @Test
    void testOneStar(Vertx vertx, VertxTestContext testContext) {
        JsonObject config = new JsonObject().put(ApplicationConfig.ENV_PIVEAU_LOAD_VOCABULARIES_FETCH, false);

        vertx.deployVerticle(MainVerticle.class.getName(), new DeploymentOptions().setConfig(config), testContext.succeeding(deploy ->
                testDqv(vertx, testContext, "one_star")));
    }

    //    @Test
    void testDqvWithExcludedValues(Vertx vertx, VertxTestContext testContext) {
        JsonObject config = new JsonObject()
                .put(ApplicationConfig.ENV_PIVEAU_LOAD_VOCABULARIES_FETCH, false)
                .put(ApplicationConfig.ENV_EXCLUDE_ANNOTATIONS, "accessRightsAvailability,byteSizeAvailability");

        vertx.deployVerticle(MainVerticle.class.getName(), new DeploymentOptions().setConfig(config), testContext.succeeding(deploy ->
                testDqv(vertx, testContext, "dqvMissingAnnotations.rdf")));
    }

    void testDqv(Vertx vertx, VertxTestContext testContext, String testDirectory) {
        Checkpoint checkpoint = testContext.checkpoint(3);

        vertx.fileSystem().readFile(testDirectory + "/dataset.rdf", testContext.succeeding(dataset ->
                vertx.fileSystem().readFile(testDirectory + "/dqv.rdf", testContext.succeeding(dqv -> {
                    vertx.createHttpServer().requestHandler(request -> {
                        if (request.method() == HttpMethod.POST && request.path().equals("/pipe")) {

                            request.bodyHandler(buffer -> {
                                Dataset expectedRdf = JenaUtils.readDataset(dqv.getBytes(), RDFMimeTypes.TRIG);

                                Pipe pipe = Json.decodeValue(buffer, Pipe.class);
                                String data = pipe.getBody().getSegments().get(1).getBody().getPayload().getBody().getData();
                                Dataset annotatedDataset = JenaUtils.readDataset(data.getBytes(), RDFMimeTypes.TRIG);

                                testContext.verify(() -> {
                                    assertTrue(annotatedDataset.containsNamedModel(TEST_NAMESPACE));
                                    Model dqvModel = annotatedDataset.getNamedModel(TEST_NAMESPACE);

                                    // Dynamic timestamps cannot be included in expected RDF.
                                    // Therefore, only their existence is tested and the according properties removed.
                                    dqvModel.listStatements(null, RDF.type, DQV.QualityMetadata).toList().stream()
                                            .findFirst().ifPresentOrElse(dqvModel::remove, () -> fail("No dqv:QualityMetadata statement found"));
                                    dqvModel.listStatements(null, PROV.generatedAtTime, (RDFNode) null).toList().stream()
                                            .findFirst().ifPresentOrElse(dqvModel::remove, () -> fail("No prov:generatedAtTime statement found"));

                                    log.info("Expected: {}", JenaUtils.write(expectedRdf.getNamedModel(TEST_NAMESPACE), Lang.TURTLE));
                                    log.info("Received: {}", JenaUtils.write(dqvModel, Lang.TURTLE));

                                    assertTrue(expectedRdf.getNamedModel(TEST_NAMESPACE).isIsomorphicWith(dqvModel));
                                    assertTrue(expectedRdf.getDefaultModel().isIsomorphicWith(annotatedDataset.getDefaultModel()));
                                });

                                checkpoint.flag();
                            });

                            request.response().setStatusCode(202).end(testContext.succeeding(ar -> checkpoint.flag()));
                        } else {
                            testContext.failNow(new Throwable("Unknown request"));
                        }
                    }).listen(8098);

                    sendPipe(vertx, testContext, checkpoint, dataset);
                }))));
    }

    private void sendPipe(Vertx vertx, VertxTestContext testContext, Checkpoint checkpoint, Buffer dataset) {
        vertx.fileSystem().readFile("pipe-stub.json", testContext.succeeding(pipeStub -> {
            JsonObject pipe = new JsonObject(pipeStub);

            // inject dataset
            pipe.getJsonObject("body")
                    .getJsonArray("segments").getJsonObject(0)
                    .getJsonObject("body")
                    .getJsonObject("payload")
                    .getJsonObject("body")
                    .put("data", dataset.toString());

            WebClient client = WebClient.create(vertx);
            client.post(8080, "localhost", "/pipe")
                    .putHeader("content-type", "application/json")
                    .expect(ResponsePredicate.SC_ACCEPTED)
                    .sendJsonObject(pipe, testContext.succeeding(response -> checkpoint.flag()));
        }));
    }
}
